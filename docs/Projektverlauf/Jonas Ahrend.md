Datum | Dauer | Tätigkeit
-------- | -------- | --------
16.10.18 | 1:30 h   | 1. Meeting
16.10.18 | 0:10 h   | Mitschriebe nachbearbeiten
16.10.18 | 0:30 h   | An Docsify verzweifeln
16.10.18 | 1:00 h   | Docsify intalliert
16.10.18 | 0:30 h   | Docsify, Porjekttagebuch Vorlage & Meeting-Protokoll angepasst & ins Repo geladen
 | | 
18.10.18 | 0:05 h   | Projektverlauf in Pages hinzugefügt
18.10.18 | 0:25 h   | Interaktive Sidebar in Pages hinzugefügt
 | | 
21.10.18 | 0:30 h   | master Branch nurnoch durch Pull-Request "mergebar" gemacht 
21.10.18 | 0:25 h   | TODO(1) Bearbeitet & an TODO(2)Verzweifelt
21.10.18 | 0:35 h   | Meeting & Vorlagen Vorbereiten
21.10.18 | 0:15 h   | Format und Rechtschreibung angepasst
21.10.18 | 1:00 h   | 1. Online Meeting
21.10.18 | 1:30 h   | Remark eingebunden
 | | 
22.10.18 | 0:20 h   | Jira angeschaut
 | | 
Summe |  8:15 h  | 
