# 1. Meeting 16.10.2018

Punkt | Entscheidung 
-------- | -------- 
Wöchendliche Treffen   | 12:30 - 15:30 (Spätestens)   
Arbeitsaufwand   | Lesen + Wöchendliche Treffen  
Wie Aufteilen   | Vorläufig macht jeder ein Kapitel, da diese jeweils ca. 50 Seiten lang sind (Ausnahme: Buch Gr.3 Kap.3 "Projekte planen" wird daher von zwei Leuten gemacht.).
Arbeitsaufteilung   | Wird Online besprochen
Online-Termin   | Son. 15:00 


# 1. Online-Meeting 21.10.2018

Punkt | Entscheidung 
-------- | -------- 
master- merge Regeln |  Nur pull-Request auf master
Branch Regel   | Möglichs häufig mit master mergen
Arbeitsaufteilung   | Aufgeteilt
Bis nächstes mal   | lesen
